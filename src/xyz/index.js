import React from 'react';
import logoJc from '../Img/react-icons.svg';

class NewPage extends React.Component {
    render(){
        return(
            <div>
                <center>
                    <img style={{width:200}} src={logoJc} alt="logo" />
                    <h4 style={{textAlign:"center"}}>
                        Hii on New Page
                    </h4>
                </center>
            </div>
        );
    }
}

export default NewPage;