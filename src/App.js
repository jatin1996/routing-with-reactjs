import React, { Component } from 'react';
import Navbar from './nav-bar.js';

class App extends Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default App;
