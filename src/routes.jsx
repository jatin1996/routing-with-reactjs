import React from 'react';
import { Route, Switch } from 'react-router-dom';
import App from './App';
import NewPage from './xyz';
import Navbar from './nav-bar';

const Routes = () => (
    <App>
        <Switch>
            <Route />
            <Route exact path="/hi" component={NewPage} />
            <Route path="*" component={Navbar} />
        </Switch>
    </App>
 )

export default Routes