import React from 'react';
import { Button } from 'reactstrap';
import logoJc from './Img/react-icons.svg';

class Navbar extends React.Component {
    routeChange() {
      window.location.href = '/hi';
    }

    render() {
      return(
        <div>
          <center>
            <img style={{width:200}}src={logoJc} alt="logo" />
            <h4>Home Page</h4>
            <button type="button" className="btn btn-primary btn-block" onClick={this.routeChange}>Click Here</button>
          </center>
        </div>
    );
  }
}

export  default Navbar;